package com.orderprocessing.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderprocessing.api.OrderResponse;
import com.orderprocessing.api.ItemInfo;
import com.orderprocessing.dao.ItemsDAO;
import com.orderprocessing.dao.OrdersDAO;
import com.orderprocessing.entity.Item;
import com.orderprocessing.entity.Order;
import com.orderprocessing.entity.OrderItem;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersDAO ordersDAO;

    @Autowired
    private ItemsDAO itemsDAO;

    @Override
    public OrderResponse getOrderById(long orderId) {
        Order order = ordersDAO.getOrderById(orderId);
        if(order==null) return null;

        Set<OrderItem> orderitems = order.getOrderItems();
        List<ItemInfo> items = new ArrayList<>();

        int totalCost = 0;
        for (OrderItem orderItem : orderitems) {
            Item item =  itemsDAO.geItemById(orderItem.getItemId());
            items.add(new ItemInfo(item.getItemName(), item.getItemCost(), orderItem.getItemCount()));

            totalCost += orderItem.getItemCount() * item.getItemCost();
        }

        return new OrderResponse(
                orderId,
                order.getCustomerName(),
                order.getPlacementDate(),
                items,
                totalCost);
    }	
}
