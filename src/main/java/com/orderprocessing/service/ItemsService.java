package com.orderprocessing.service;

import com.orderprocessing.entity.Item;

public interface ItemsService {

    Item getItemById(long itemId);

}
