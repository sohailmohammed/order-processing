package com.orderprocessing.service;

import com.orderprocessing.api.OrderResponse;

public interface OrdersService {
     OrderResponse getOrderById(long orderId);
}
