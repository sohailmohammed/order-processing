package com.orderprocessing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderprocessing.dao.ItemsDAO;
import com.orderprocessing.entity.Item;
@Service
public class ItemsServiceImpl implements ItemsService {

    @Autowired
    private ItemsDAO itemsDAO;

    @Override
    public Item getItemById(long itemId) {
        Item items  = itemsDAO.geItemById(itemId);
        return items;
    }	
}
