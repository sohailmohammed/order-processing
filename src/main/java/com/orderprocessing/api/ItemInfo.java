package com.orderprocessing.api;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
@SuppressWarnings("serial")
public class ItemInfo implements Serializable {

    private String itemName;
    private int itemCost;
    private int itemCount;

}