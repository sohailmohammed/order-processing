package com.orderprocessing.api;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
@SuppressWarnings("serial")
public class OrderResponse implements Serializable {

    private long orderId;
    private String customerName;
    private String orderedDate;
    private List<ItemInfo> items;
    private int totalCost;

}