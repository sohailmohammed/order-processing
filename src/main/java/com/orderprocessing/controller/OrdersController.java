package com.orderprocessing.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orderprocessing.api.OrderResponse;
import com.orderprocessing.service.OrdersService;

@Controller
@EnableAutoConfiguration
@RequestMapping(path="orders_app/orders")
public class OrdersController {

    @Autowired
    private OrdersService orderService;

    @GetMapping(path="{order_id}")
    public ResponseEntity<OrderResponse> getOrderDetailsById(@PathVariable("order_id") Long orderid) {
        OrderResponse orderResp = orderService.getOrderById(orderid);
        return orderResp==null ? new ResponseEntity<OrderResponse>(orderResp, HttpStatus.NOT_FOUND) :
            new ResponseEntity<OrderResponse>(orderResp, HttpStatus.OK);
    }

} 