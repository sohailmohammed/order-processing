package com.orderprocessing.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Entity
@Table(name = "items")
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "item_id", unique = true, nullable = false)
    private long itemId;
    
    @Column(name = "item_name", unique = true, nullable = false)
    private String itemName;
    
    @Column(name = "item_cost")
    private int itemCost;

    public Item(String itemName, int itemCost) {
        this.itemName = itemName;
        this.itemCost = itemCost;
    }

}