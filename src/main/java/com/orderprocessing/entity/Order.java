package com.orderprocessing.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Entity
@Table(name = "orders")
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", unique = true, nullable = false)
    private long orderId;
    
    @Column(name = "placement_date", nullable = false)
    private String placementDate;
    
    @Column(name = "customer_name")
    private String customerName;
    
    @OneToMany(targetEntity = OrderItem.class, mappedBy = "order")
    private Set<OrderItem> orderItems;

    public Order(String placementDate, String customerName, Set<OrderItem> orderItems) {
        this.placementDate = placementDate;
        this.customerName = customerName;
        this.orderItems = orderItems;
    }

}