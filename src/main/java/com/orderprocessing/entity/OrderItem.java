package com.orderprocessing.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Entity
@Table(name = "order_items")
@NoArgsConstructor
@Setter
@Getter
@ToString
public class OrderItem implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "order_item_id", unique = true, nullable = false)
    private long orderItemId;
    
    @ManyToOne
    @JoinColumn(name = "order_id", nullable=false)
    private Order order;
    
    @Column(name = "item_id", nullable=false)
    private long itemId;
    
    @Column(name = "item_count", nullable=false)
    private int itemCount;

    public OrderItem(int itemId, int itemCount) {
        this.itemCount = itemCount;
        this.itemId = itemId;
    }

}