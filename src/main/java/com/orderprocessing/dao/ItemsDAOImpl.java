package com.orderprocessing.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.orderprocessing.entity.Item;

@Transactional
@Repository
public class ItemsDAOImpl implements ItemsDAO {

    @PersistenceContext	
    private EntityManager entityManager;	

    @Override
    public Item geItemById(long itemId) {
        return entityManager.find(Item.class, itemId);
    }
}
