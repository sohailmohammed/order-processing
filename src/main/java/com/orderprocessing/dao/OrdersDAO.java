package com.orderprocessing.dao;

import com.orderprocessing.entity.Order;

public interface OrdersDAO {

    Order getOrderById(long orderid);

}