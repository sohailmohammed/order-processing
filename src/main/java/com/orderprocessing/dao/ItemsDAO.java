package com.orderprocessing.dao;
import com.orderprocessing.entity.Item;

public interface ItemsDAO {

    Item geItemById(long itemId);

}
