package com.orderprocessing.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.orderprocessing.entity.Order;

@Transactional
@Repository
public class OrdersDAOImpl implements OrdersDAO {

    @PersistenceContext	
    private EntityManager entityManager;	

    @Override
    public Order getOrderById(long orderId) {
        return entityManager.find(Order.class, orderId);
    }

}
